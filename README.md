# BSD scripts

A collection of some BSD scripts that I use.

This is public domain, if you find any use from any of these scripts, good!

They are not also guaranteed to be up to date, I often get new scripts and
slightly modify scripts and they do not live in this repository.

## VMM Scripts:

 * `9front`
 * `alpine`
 * `ubuntu`
 
These three scripts allow for toggling on and off virtual machines, as well as
entering their console and SSHing in. They are for easier access to the virtual
machine controls.
 
## Starting Scripts:

 * `acmestart`
 * `drawtermstart`
 * `infernostart`
 
These three scripts are to make opening acme, drawterm and inferno easier, it
means that you do not have to remember very specific locations of fonts and
files.
 
## Misc Scripts:

 * `bar`
 * `emoji`
 * `ime`
 * `setbg`
 * `togglebg`
 * `togglecomp`
 * `tether`
 * `temple`
 * `quickload-bookmarks`

`bar` is a bar for dwm, I may consider rewriting it in C in the future.

`emoji` brings up a list of emoji in dmenu, copies the selected to the clipboard.

`ime` is a script for toggling the IME on and off.

`setbg` allows you to change the bg to any provided argument, or to the defualt
if there is no provided argument.

`togglebg` toggles the live wallpaper (xlivebg).

`togglecomp` toggles the compositor (picom).

`tether` allows for phone tethering for internet access.

`temple` allows one to access TempleOS, starts it in qemu.

`quickload-bookmarks` allows one to quickly load bookmarks from a plaintext file,
opening them in whatever browser they want.
